package practice;
public class Food{
	private String name;
	private double price;
	private double weight;
	public Food(){
		this("nothing",0.0,0.0);
	}
	public Food(String name){
		this(name,0.0,0.0);
	}
	public Food(String name, double price){
		this(name,price,0.0);
	}
	public Food(String name,double price, double weight){
		this.name = name;
		this.price = price;
		this.weight = weight;
	}
	public String getName(){
		return name;
	}
	public double getPrice(){
		return price;
	}
	public double getWeight(){
		return weight;
	}
	public int compareByName(Food f){
		return this.name.toLowerCase().charAt(0)- f.getName().toLowerCase().charAt(0);
	}
	public double compareByPrice(Food f){
		return this.price - f.price;
	}
	public double compareByWeight(Food f){
		return this.weight - f.weight;
	}
	public String toString(){
		return name+" weights "+ weight+" lbs, costs "+price;
	}
	@Override
	public boolean equals(Object f){
		System.out.println("equals method called");
		if(f==null)
			return false;
		else
			if(f instanceof Food)
				return this.compareByName((Food)f)==0 && this.compareByPrice((Food)f)==0.0 && this.compareByWeight((Food)f) == 0.0;
			else
				return false;
	}
}
