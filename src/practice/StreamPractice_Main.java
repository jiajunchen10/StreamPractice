package practice;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
public class StreamPractice_Main {

	public static void main(String[] args) {
		List<Food> foodList= new ArrayList<>(); 
		Food duplicated = new Food("coke",1.05,1);
		foodList.add(new Food("fries",2.99,0.3));   // food name, food price, food weight
		foodList.add(new Food("cheese burger",4.99,0.6));
		foodList.add(new Food("onion rings",2.99,0.3));
		foodList.add(new Food("chicken wings",7.50,0.7));
		foodList.add(new Food("cheese cake",3.4,0.3));
		foodList.add(new Food("cheese pizza",8.99,0.3));
		foodList.add(duplicated);
		foodList.add(new Food("ice cream",3.49,0.2));
		foodList.add(duplicated);      // duplicated item
		foodList.add(new Food());   // 
		foodList.add(new Food("water"));            // free item, weight nothing
		foodList.add(new Food("water bottle",0.01,0.0)); // free item, has weight
		
		Comparator<Food> byPrice = new Comparator<Food>(){
			@Override
			public int compare(Food o1, Food o2) {
				return o1.compareByPrice(o2)>0? 1: o1.compareByPrice(o2)==0? 0: -1;
			}
		};
		Comparator<Food> byName = new Comparator<Food>(){
			@Override
			public int compare(Food o1, Food o2) {
				return o1.compareByName(o2);
			}
		};
		Comparator<Food> byWeight = new Comparator<Food>(){
			@Override
			public int compare(Food o1, Food o2) {
				return o1.compareByWeight(o2)>0? 1: o1.compareByWeight(o2)==0? 0: -1;
			}
		};
		//sorting the food List by name
		System.out.println("sorting the food List by name: \n");
		foodList.stream().sorted(byName).forEach(System.out::println);
		System.out.println("=======================================================");
		
		// sorting the food List by price
		System.out.println("\n\nsoring the food list by price: \n");
		foodList.stream().sorted(byPrice).forEach((x)->System.out.println(x));
		System.out.println("=======================================================");
		
		// searching for any free food
		System.out.println("\n\nsearch for any free food: \n");
		foodList.stream().filter((x)-> x.getPrice()==0).forEach(System.out::println);
		System.out.println("=======================================================");

		// search for 5 cheapest food, includes the free foods
		System.out.println("\n\nsearch for 5 cheapest food, includes the free foods: \n");
		foodList.stream().filter(x->x.getPrice()!=0).sorted(byPrice).limit(5).forEach(System.out::println);
		System.out.println("=======================================================");
		
		// search for the cheapest food, excludes free food.
		System.out.println("\n\nsearch for cheapest foods that are not free: \n");
		foodList.stream().filter(x->x.getPrice()!=0).min(byPrice).ifPresent(System.out::println);
		System.out.println("=======================================================");
		
		// search for food <= 2 dollar
		System.out.println("\n\nsearch for food names that are less than or equal to $2: \n");
		foodList.stream().distinct().filter(x->x.getPrice()<=2).map(x->x.getName()).forEach(System.out::println);
		System.out.println("=======================================================");
		
		// search for heaviest food
		System.out.println("\n\nsearch for heaviest food: \n");
		foodList.stream().distinct().max(byWeight).ifPresent(System.out::println);
		System.out.println("=======================================================");
		
		// sort and collect all the food names to an arraylist of Strings and then print out.
		System.out.println("\n\nsort and collect all the food names to an arraylist of Strings and then print out: \n");
		foodList.stream().sorted(byName).map(x->x.getName()).collect(Collectors.toCollection(ArrayList::new)).forEach(System.out::println);
		System.out.println("=======================================================");
		
		// find food names start with c, then print the names and the corresponding prices.
		Function<Food, String> printPrice = new Function<Food, String>(){
			@Override
			public String apply(Food t) {
				return t.getName()+" is "+ t.getPrice()+" dollars";
			}
		};
		
		System.out.println("\n\nfind food names start with c, then print the names and the corresponding prices.\n");
		foodList.stream().filter(x->x.getName().startsWith("c")).map(x->x.getName()+" is "+ x.getPrice()+" dollars").forEach(System.out::println);
	}
}
